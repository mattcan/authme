'use strict'

const path = require('path')
const { kCsrfToken } = require('../../lib/constants')

const schema = {
  querystring: {
    type: 'object',
    required: ['me', 'client_id', 'redirect_uri'],
    properties: {
      me: { type: 'string' },
      client_id: { type: 'string' },
      redirect_uri: { type: 'string' },
      state: { type: 'string' },
      response_type: { type: 'string', enum: ['id', 'code'] },
      scope: { type: 'string' }
    }
  }
}

module.exports = async function (fastify, opts) {
  fastify.get('/', { schema }, async function (request, reply) {
    const { client_id: clientId, redirect_uri: redirectUri, me, state, scope } = request.query
    const scopes = scope ? scope.split(' ') : []

    // TODO check scope/responsetype
    reply.view(
      path.join('templates', 'index.ejs'),
      { clientId, redirectUri, me, state, scopes, _csrf: request[kCsrfToken] }
    )
    return reply
  })

  fastify.post('/', async function (request, reply) {
    request.log.debug({ token: request[kCsrfToken], cookieKeys: Object.keys(request.cookies) }, 'debugging data')
    return { some: 'data' }
  })
}
