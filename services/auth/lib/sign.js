'use strict'

const crypto = require('crypto')
const { ONEYEAR } = require('./constants')

module.exports = function (key, data, ttl = ONEYEAR) {
  const expires = Date.now() + (ttl * 1000)
  const body = `${data}${expires}`
  const signature = crypto.createHmac('sha256', key).update(body).digest('hex')
  
  return `${expires.toString(16)}:${signature}`
}
