'use strict'

const pov = require('point-of-view')

pov.autoConfig = {
  engine: {
    ejs: require('ejs')
  }
}

module.exports = pov
