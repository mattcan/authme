'use strict'

const Tokens = require('csrf')
const fp = require('fastify-plugin')
const { TWOMINUTES, kCsrfToken } = require('../../lib/constants')

async function csrfHooks (fastify, opts) {
  fastify.addHook('preHandler', async (request, reply) => {
    const { _csrf } = request.cookies

    let token = _csrf
    if (!token) {
      // generate a token
      const tokens = new Tokens()
      token = tokens.create(await tokens.secret())

      // add to cookie
      reply.setCookie('_csrf', token, {
        // domain: '127.0.0.1',
        path: '/',
        maxAge: TWOMINUTES,
        // httpOnly: true,
        sameSite: 'strict',
        secure: false // TODO should be true
      })
    }

    request[kCsrfToken] = token
  })

  fastify.addHook('preHandler', async function (request, reply) {
    const { raw: { method }, body, cookies } = request

    if (method !== 'POST') { return }

    if (body._csrf === cookies._csrf) { return }

    reply.status(400)
    reply.view('/templates/error.ejs', { title: 'Bad Request', message: 'CSRF tokens do not match' })
    return reply
  })
}

module.exports = fp(csrfHooks, {
  fastify: '2.x',
  name: 'csrf',
  dependencies: ['fastify-cookie', 'fastify-sensible'],
  decorators: {
    request: ['cookies']
  }
})
