'use strict'

module.exports.ONEYEAR = 31536000
module.exports.TWOMINUTES = 120
module.exports.kCsrfToken = Symbol.for('csrftoken')
