# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

* preHandler hook to generate a new csrf token and set a response cookie. The
  token is then set on the request object so that it can be passed to the form
* preHandler hook to validate CSRF token
* an endpoint to display the form
* an endpoint to read the form data
